'''
Created on May 30, 2019

@author: jfpiolle
'''
import logging
from opensearch import Client, Query


logging.basicConfig(level=logging.DEBUG)


class OpensearchServer():

    def search(self, datasetId, timeStart=None, timeEnd=None, geoBox=None):

        # translate keywords
        search_args = {"itemsPerPage": 100}
        search_args[self.PARAMS['datasetId']] = self.get_dataset_id(datasetId)
        if timeStart is not None:
            search_args[self.PARAMS['timeStart']] = timeStart
        if timeEnd is not None:
            search_args[self.PARAMS['timeEnd']] = timeEnd
        if geoBox is not None:
            search_args[self.PARAMS['geoBox']] = geoBox

        tmp = self.client.search(self.END_POINT, **search_args)

        result = {}
        for item in tmp:
            gid = self.get_granule(item)
            if gid not in result:
                result[gid] = {}
            result[gid].update(self.get_access(item))
        return result

    def get_dataset_id(self, datasetId):
        if datasetId in self.CATALOG:
            return self.CATALOG[datasetId]
        return datasetId


class CWWICServer(OpensearchServer):

    OSDD_URL = "https://cwic.wgiss.ceos.org/opensearch/datasets/osdd.xml?clientId=cwicClient"
    END_POINT = 'granules'
    PARAMS = {'datasetId': 'dc__identifier', 'timeStart': 'time__start',
              'timeEnd': 'time__end', 'geoBox': 'geo__box'}

    CATALOG = {
        "UKMO-L4HRfnd-GLOB-OSTIA_1.0": "UKMO-L4HRfnd-GLOB-OSTIA_1.0",
        "JPL-L2P-MODIS_A_1.0": "JPL-L2P-MODIS_A_1.0",
        "AVHRR_SST_METOP_B-OSISAF-L2P-v1.0": "gov.noaa.nodc:GHRSST-AVHRR_SST_METOP_B-OSISAF-L2P_1",
        "NEODAAS-L2P-AVHRR19_L": "NEODAAS-L2P-AVHRR19_L_1"
    }

    def __init__(self, osdd_url=OSDD_URL):
        self.client = Client(osdd_url)

    def get_granule(self, item):
        return '.'.join(item['title'].split(':')[-1].split('.')[1:]).split('.nc')[0] + '.nc'

    def get_access(self, item):
        access = {}
        for link in item['links']:
            if link['title'] == 'FTP':
                access['FTP'] = link['href']
            elif link['title'] == 'HTTP':
                access['HTTP'] = link['href']
            elif link['title'] == 'OPeNDAP THREDDS':
                access['OPeNDAP'] = link['href']
            elif link['title'] == 'THREDDS(TDS)':
                access['THREDDS'] = link['href']
        return access


class PODAACServer(OpensearchServer):

    OSDD_URL = "https://podaac.jpl.nasa.gov/ws/search/granule/osd.xml"
    END_POINT = 'granule'
    PARAMS = {'datasetId': 'podaac__datasetId', 'timeStart': 'time__start',
              'timeEnd': 'time__end', 'geoBox': 'georss__box'}

    CATALOG = {
        "UKMO-L4HRfnd-GLOB-OSTIA_1.0": "PODAAC-GHOST-4FK01",
        "JPL-L2P-MODIS_A_1.0": "PODAAC-GHMDA-2PJ02",
        "AVHRR_SST_METOP_B-OSISAF-L2P-v1.0": "PODAAC-GHAMB-2PO02",
        "NEODAAS-L2P-AVHRR19_L": "PODAAC-GH19L-2PS01"
    }

    def __init__(self, osdd_url=OSDD_URL):
        self.client = Client(osdd_url)

    def get_granule(self, item):
        return item['dcterms_identifier'].split(':')[1].split('.nc')[0] + '.nc'

    def get_access(self, item):
        access = {}
        for link in item['links']:
            if link['title'] == 'FTP URL':
                access['FTP'] = link['href']
            elif link['title'] == 'HTTP URL':
                access['HTTP'] = link['href']
            elif link['title'] == 'OPeNDAP URL':
                access['OPeNDAP'] = link['href']
        return access


class GHRSSTServer(OpensearchServer):
    def __init__(self):
        self.dacs = {
            "PODAAC": PODAACServer(),
            "NOAA": CWWICServer()
        }

    def search(self, datasetId, timeStart=None, timeEnd=None, geoBox=None):
        result = {}
        for dac in self.dacs:
            logging.info("Fetching DAC: {}".format(dac))
            result[dac] = self.dacs[dac].search(datasetId, timeStart=timeStart,
                                                timeEnd=timeEnd, geoBox=geoBox)

        return result

    def display(self, result, protocol=None):
        granules = set()
        for dac in result:
            granules = granules.union(set(result[dac].keys()))
        granules = list(granules)
        granules.sort()

        # print result
        print "AGGREGATED RESULT"
        print "-----------------"
        for granule in granules:
            print "- ", granule
            for dac in result:
                if granule not in result[dac]:
                    continue
                print "    %s: " % dac
                if protocol is None:
                    for p in result[dac][granule]:
                        print "        ", p, ':', result[dac][granule][p]


if __name__ == "__main__":
    client = GHRSSTServer()

    result = client.search(datasetId="NEODAAS-L2P-AVHRR19_L",
                           timeStart="2010-05-06", timeEnd="2010-05-07",
                           geoBox='-180,-90,180,90')
    client.display(result)
