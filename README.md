# rgtsdemo

Demonstrates a federated opensearch query to mimic the next GHRSST R/G TS central opensearch access. It connects to different opensearch services, queries the same result and prints the merged result.


Prerequisites:

you need to install *opensearch* package:


   pip install opensearch


Then run the example:

   python bin/federated_query_ex.py

